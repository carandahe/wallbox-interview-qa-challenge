module.exports = {
// user and charger location
    user: {
        name:'User Code Test',
        email: 'wallbox.auto+codeText@gmail.com',
        password: 'WBXcode321',
    },
// Onboarding
    onboardingTitle: 'Welcome to the best EV charging experience',
    onboardingMessage: 'myWallbox helps you easily manage and monitor your charging.',
    onboardingAddChargerTitle: 'Add your charger',
    onboardingAddChargerMessage: 'First, link your charger using the myWallbox app.',
    onboardingConfigureChargerTitle: 'Configure your charger',
    onboardingConfigureChargerMessage: 'Once linked, use the myWallbox app to configure your charger settings.',
    onboardingControlChargerTitle: 'Control the experiences',
    onboardingControlChargerMessage: 'Use the myWallbox app to remotely control your charger, including locking/unlocking and charge session scheduling. You can also use the myWallbox app to access charger statistics in real-time.',
    onboardingStartButton: 'START',
}
