import AppScreen from './app.screen';

const SELECTORS = {
    Wallbox_Logo: AppScreen.androidUISelectorResourceId('imgLoginLogo'),
    Continue_With_Email: AppScreen.androidUISelectorResourceId('btnContinueWithEmail'),
    Email_Field: AppScreen.androidUISelectorResourceIdfromParent('txfEmail','tiet'),
    Password_Field: AppScreen.androidUISelectorResourceIdfromParent('txfPassword','tief'),
    Login_Button: AppScreen.androidUISelectorResourceId('btnEnter')
};

class LoginScreen extends AppScreen {
    constructor () {
        super(SELECTORS.Wallbox_Logo);
    }

    get email () {
        return $(SELECTORS.Email_Field);
    }

    get password () {
        return $(SELECTORS.Password_Field);
    }

    get enterButton () {
        return $(SELECTORS.Login_Button);
    }

    getElement(selectorKey) {
        return $(SELECTORS[selectorKey]);
    }

    doLogin(user) {
        this.email.setValue(user.email);
        this.password.setValue(user.password);
        this.enterButton.click();
    }

}

export default new LoginScreen();
