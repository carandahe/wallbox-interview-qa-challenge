export default class AppScreen {
    constructor (selector) {
        this.selector = selector;
    }

    static androidId (id) {
        return browser.capabilities.appPackage + ":id/" + id;
    }

    static androidUISelectorResourceId (id) {
        return 'android=new UiSelector().resourceId("' + this.androidId(id) + '")';
    }

    static androidUISelectorResourceIdfromParent (parentId, id) {
        return 'android=new UiSelector().resourceId("' + this.androidId(parentId) + '").childSelector(UiSelector().resourceId("' + this.androidId(id) + '"))';
    }

    /**
     * Wait for the login screen to be visible
     *
     * @param {boolean} isShown
     * @return {boolean}
     */
    waitForIsShown (isShown = true) {
        return $(this.selector).waitForDisplayed({
            timeout: 30000,
            reverse: !isShown,
        });
    }
}
