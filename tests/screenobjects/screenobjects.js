import loginScreen from "./login.screen";
import onboardingScreen from "./onboarding.screen";
import homeScreen from "./home.screen";

const screenObjects = {

    LoginScreen: loginScreen,
    OnboardingScreen: onboardingScreen,
    HomeScreen: homeScreen
};

export default screenObjects;
