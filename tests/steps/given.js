import { Given } from '@cucumber/cucumber';
import glovar from '../testData/executionVariables';
import screenObjects from '../screenobjects/screenobjects';

// ##################################################### LOGIN #######################################################

Given(/^I am in the login screen of the Wallbox App$/, () => {
    screenObjects['LoginScreen'].waitForIsShown(true);
});

Given(/^I login with existing user$/, function () {
    try {
        screenObjects['LoginScreen'].getElement('Continue_With_Email').click();
    } catch (error) {
        console.info('Social Login not shown')
    }
    screenObjects['LoginScreen'].doLogin(glovar.user);
});


