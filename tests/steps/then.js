import { Then } from '@cucumber/cucumber';
import glovar from '../testData/executionVariables';
import screenObjects from '../screenobjects/screenobjects';

// ##################################################### LOGIN #######################################################

Then( /^I expect that element "([^']*)" from "([^']*)" is( not)* displayed$/, function (element, screen, falseCase){    
    if (falseCase) {
        expect(screenObjects[screen].getElement(element).isDisplayed()).toEqual(false)
    } else {

        expect(screenObjects[screen].getElement(element)).toBeDisplayed();
    }
});

Then(/^I expect that element "([^"]*)" from "([^"]*)" matches the text "([^"]*)"$/, function (element, screen, text){
    expect(screenObjects[screen].getElement(element)).toHaveText(glovar[text]);
});
