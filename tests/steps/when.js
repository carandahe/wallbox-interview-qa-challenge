import { When } from '@cucumber/cucumber';
import Gestures from '../helpers/Gestures';
import glovar from '../testData/executionVariables';
import screenObjects from '../screenobjects/screenobjects';

// ##################################################### LOGIN #######################################################

When(/^I login with the execution user$/, function () {
    screenObjects['LoginScreen'].doLogin(glovar.user);
});

When(/^I (click|tap) on the element "([^"]*)" from "([^"]*)"$/, function (action, element, screen) {
    switch (action) {
        case 'click':
            screenObjects[screen].getElement(element).click();
            break;
        case 'tap':
            Gestures.tapOnElement(screenObjects[screen].getElement(element));
            break;
    }
});

When(/^I try to click on the element "([^"]*)" from "([^"]*)"$/, function (element, screen) {
    try {
        screenObjects[screen].getElement(element).click();
    } catch (error) {
        console.info('Element ' + element + ' was not present in the screen ' + screen );
    }
});

When(/^I accept location permisions and deny enabling bluetooth if required$/, function () {
    if (driver.isAndroid){
        try {
            screenObjects['HomeScreen'].getElement('Allow_Location').click();
        } catch (error) {
            console.info('Location permisions not asked')
        }
    }
    else{
        try {
            screenObjects['HomeScreen'].getElement('Deny_Enabling_Bluetooth').click();
        } catch (error) {
            console.info('Location permisions not asked')
        }
    }
    driver.pause(3000);
});
