@Apps
Feature: Login test - base scenarios
    As a User
    I want to be able to login into Wallbox App and see the onboarding

    Scenario: LOG Login with correct credentials
        Given I am in the login screen of the Wallbox App
        When  I login with existing user
        Then  I expect that element "Onboarding_Title" from "OnboardingScreen" is displayed

    Scenario: LOG Onboarding flow after first login
        Then  I expect that element "Onboarding_Title" from "OnboardingScreen" matches the text "onboardingTitle"
        And   I expect that element "Onboarding_Message" from "OnboardingScreen" matches the text "onboardingMessage"
        When  I tap on the element "Next_Button" from "OnboardingScreen"
        Then  I expect that element "Onboarding_Title" from "OnboardingScreen" matches the text "onboardingAddChargerTitle"
        And   I expect that element "Onboarding_Message" from "OnboardingScreen" matches the text "onboardingAddChargerMessage"
        When  I tap on the element "Next_Button" from "OnboardingScreen"
        Then  I expect that element "Onboarding_Title" from "OnboardingScreen" matches the text "onboardingConfigureChargerTitle"
        And   I expect that element "Onboarding_Message" from "OnboardingScreen" matches the text "onboardingConfigureChargerMessage"
        When  I tap on the element "Next_Button" from "OnboardingScreen"
        Then  I expect that element "Onboarding_Title" from "OnboardingScreen" matches the text "onboardingControlChargerTitle"
        And   I expect that element "Onboarding_Message" from "OnboardingScreen" matches the text "onboardingControlChargerMessage"
        When  I tap on the element "Close_Button" from "OnboardingScreen"
        And   I accept location permisions and deny enabling bluetooth if required
        Then  I expect that element "Add_Charger_Button" from "HomeScreen" is displayed
