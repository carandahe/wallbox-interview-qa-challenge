# Setting up Android and iOS on a local machine
To setup your local machine to use an Android emulator and an iOS simulator see [Setting up Android and iOS on a local machine](./docs/ANDROID_IOS_SETUP.md)

# Quick start
Choose one of the following options:

1. Clone the git repo 

2. Rum `npm install`

3. Add your phone name and os version in the capabilities

3. Run the tests for Android with `npx wdio run ./config/wdio.android.local.conf.js --suite login`

4. To run a especific spec file `npx wdio run ./config/wdio.android.local.conf.js --spec LOG-login&onboarding` 

# Exercise

1. We have one feature with two escenarios but it's not working, since the login is included in one function we can't figure out where it's failing, please help us identify the issue and fix it so the current feature runs.

2. There is so few coverage of the app, help us add more tests to the login so it's more robust.

3. We'll like to cover the register as well, try to add tests for the register escenarios.