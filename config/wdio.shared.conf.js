exports.config = {
    // ====================
    // Runner and framework
    // Configuration
    // ====================
    runner: 'local',
    framework: 'cucumber',
    sync: true,
    logLevel: 'info',
    deprecationWarnings: true,
    bail: 0,
    waitforTimeout: 10000,
    connectionRetryTimeout: 600000,
    connectionRetryCount: 1,
    reporters: [
        'spec',
        [ 'cucumberjs-json', {
            jsonFolder: 'results/features/',
            language: 'en',
        }],
    ],
    cucumberOpts: {
        backtrace: false,
        requireModule: ['@babel/register'],
        failAmbiguousDefinitions: true,
        failFast: false,
        ignoreUndefinedDefinitions: false,
        names: [],
        snippets: true,
        format: ['pretty'],
        colors: true,
        source: true,
        profile: [],
        require: [
            './tests/steps/*.js',
        ],
        scenarioLevelReporter: false,
        order: 'defined',
        snippetSyntax: undefined,
        strict: true,
        // <string> (expression) only execute the features or scenarios with
        // tags matching the expression, see
        // https://docs.cucumber.io/tag-expressions/
        tagExpression: '@Apps',
        // <boolean> add cucumber tags to feature or scenario name
        tagsInTitle: false,
        timeout: 360000,
    },
    
    specs: [
        './tests/features/**/*.feature',
    ],

    suites: {
        login: [
            './tests/features/APP/**/LOG*.feature',
        ]
    },

    // ====================
    // Appium Configuration
    // ====================
    services: [
        [
            'appium',
            {
                args: {
                    relaxedSecurity: true,
                },
                command: 'appium',
                logPath: './'
            },
        ],
    ],
    port: 4723,
};
