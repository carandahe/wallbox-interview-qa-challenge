const { config } = require('./wdio.shared.conf');


// ============
// Capabilities
// ============

config.capabilities = [
    {
        platformName: 'Android',
        maxInstances: 1,
        'appium:deviceName': '###', // ADD your phone name
        'appium:platformVersion': '##', // ADD your os version
        'appium:automationName': 'UiAutomator2',
        'appium:autoGrantPermissions': true,
        'appium:appPackage': 'com.wallbox',
        'appium:appActivity': 'com.wallbox.splash.presentation.SplashActivity',
        'appium:newCommandTimeout': 240,
    },
];

exports.config = config;
